<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('heros', function (Request $request) {
   $heros = '[
  {
    "name": "Ahmed",
    "mobile": "999",
    "id": 1
  },
  {
    "name": "Sami",
    "mobile": "999",
    "id": 2
  },
  {
    "name": "Sara",
    "mobile": "999",
    "id": 3
  },
  {
    "name": "Susan",
    "mobile": "999",
    "id": 4
  },
  {
    "name": "Saud",
    "mobile": "555",
    "id": 5
  },
  {
    "name": "Sad",
    "mobile": "4324324",
    "id": 6
  }
]';

    return $heros;
});

Route::get('config',function(){
    $config = '[
  {
    "title": "Email",
    "type": "text"
  },
  {
    "title": "Phone",
    "type": "text"
  },
  {
    "title": "Name",
    "type": "text"
  },
  {
    "title": "Company",
    "type": "text"
  },
  {
    "title": "country",
    "type": "dropdown",
    "api": "http://countryapi.gear.host/v1/Country/getCountries?pLimit=25&pPage=1",
    "multiple": false
  },
  {
    "title": "Date",
    "type": "date"
  }
]';

  return $config;
});
Route::get('countries',function (){
$countries = '{
  "IsSuccess": true,
  "UserMessage": null,
  "TechnicalMessage": null,
  "TotalCount": 2,
  "Response": [
    {
      "Name": "Afghanistan",
      "Alpha2Code": "AF",
      "Alpha3Code": "AFG",
      "NativeName": "افغانستان",
      "Region": "Asia",
      "SubRegion": "Southern Asia",
      "Latitude": "33",
      "Longitude": "65",
      "Area": 652230,
      "NumericCode": 4,
      "NativeLanguage": "pus",
      "CurrencyCode": "AFN",
      "CurrencyName": "Afghan afghani",
      "CurrencySymbol": "؋",
      "Flag": "https://api.backendless.com/2F26DFBF-433C-51CC-FF56-830CEA93BF00/473FB5A9-D20E-8D3E-FF01-E93D9D780A00/files/CountryFlags/afg.svg",
      "FlagPng": "https://api.backendless.com/2F26DFBF-433C-51CC-FF56-830CEA93BF00/473FB5A9-D20E-8D3E-FF01-E93D9D780A00/files/CountryFlagsPng/afg.png"
    },
    {
      "Name": "Åland Islands",
      "Alpha2Code": "AX",
      "Alpha3Code": "ALA",
      "NativeName": "Åland",
      "Region": "Europe",
      "SubRegion": "Northern Europe",
      "Latitude": "60.116667",
      "Longitude": "19.9",
      "Area": 1580,
      "NumericCode": 248,
      "NativeLanguage": "swe",
      "CurrencyCode": "EUR",
      "CurrencyName": "Euro",
      "CurrencySymbol": "€",
      "Flag": "https://api.backendless.com/2F26DFBF-433C-51CC-FF56-830CEA93BF00/473FB5A9-D20E-8D3E-FF01-E93D9D780A00/files/CountryFlags/ala.svg",
      "FlagPng": "https://api.backendless.com/2F26DFBF-433C-51CC-FF56-830CEA93BF00/473FB5A9-D20E-8D3E-FF01-E93D9D780A00/files/CountryFlagsPng/ala.png"
    }
  ]
}';

return $countries; 
});