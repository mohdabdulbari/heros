require('./bootstrap');

window.Vue = require('vue').default;


Vue.component('heros-form', require('./components/HerosForm.vue').default);
Vue.component('hero', require('./components/Hero.vue').default);


const app = new Vue({
    el: '#app',
});
