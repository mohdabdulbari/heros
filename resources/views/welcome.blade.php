<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Heros Form</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <style>
        body{
            margin: 150px 0;
            background: #eef0f3;
        }
        table{
            background: #fff;
        }
        .filters-form{
            background: #fff;
            text-align: center;
           padding: 20px;
        }
    </style>
</head>
<body>
    <div id="app">
        <heros-form></heros-form>
    </div>
    <script src="{{asset('js/app.js')}}"></script>
</body>
</html>